# Docker para Aplicación PHP + Apache + MariaDB + PHPMyAdmin

Consta de 3 contenedores:

- MariaDB
- PhpMyAdmin
- PHP 7  +  Apache

Genera 2 volúmenes para datos y 1 para phpmyadmin:

- **data**: Para base de datos.
- **html**: Para los archivos PHP.
- **sessions**: Para PhpMyAdmin.

## Configuración

Existe el archivo `.env` donde podremos configurar los valores:

Cambia los valores, si crees necesario ...

- MYSQL_ROOT_PASSWORD=123456
- MYSQL_DATABASE=name_db
- MYSQL_USER=myuser
- MYSQL_PASSWORD=123456
- PORT_DB=3311
- PORT_PHPMYADMIN=8011
- PORT_APP=8080

En el directorio **html** hay un archivo con `info.php` , en ese mismo archivo hay una conexión a la base de datos para comprobar la conectividad.

La prueba de conexión del archivo `info.php` tiene los valores de prueba, pon  los que sean necesarios.

También hay creado un `index.php` dónde podremos comprobar el funcionamiento de php y la conectividad con la Base de datos.

### Cambiar la versión de PHP

Dentro del directorio `Docker` hay varios directorios que almacenan cada Dockerfile con su PHP versión y configuraciones. Luego se ha de especificar en el archivo **docker-compose.yml** en la fila que hace referencia a la configuración del PHP.

```
...
build: ./Docker/php-7-2-apache/
...
```



### Ejecutar

Primera vez .... `docker-compose build`

`docker-compose up -d`




### Accesible desde ...

`http://localhost:8080`


# Tags para PHP  ...

[Docker Hub PHP](https://hub.docker.com/_/php/)

[Docker Hub MariaDB](https://hub.docker.com/_/mariadb)

### LOGS

 - [**15-7-2020**] : En Apliciones PHP, ahora el PHP carga y habilita mcrypt

Ahora la versión de PHP también carag alguna configuraciones en como el método rewrite, gd, zip etc. Necesarias para proyectos de tipo Prestashop por ejemplo.
