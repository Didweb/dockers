# Docker para Prestashop

Consta de 3 contenedores:

- MariaDB
- PhpMyAdmin
- Prestashop 1.7 + php 7.2 + Apache

Genera 2 volúmenes para datos y 1 para phpmyadmin:

- **data**: Para base de datos.
- **html**: Para los archivos de Prestashop.
- **sessions**: Para PhpMyAdmin.


## Configuración

Existe el archivo `.env` donde podremos configurar los valores:

Cambia los valores, si crees necesario ...

- MYSQL_ROOT_PASSWORD=123456
- MYSQL_DATABASE=name_db
- MYSQL_USER=myuser
- MYSQL_PASSWORD=123456
- PORT_DB=3311
- PORT_PHPMYADMIN=8011
- PORT_PRESTASHOP=8080
- TAG_PRESTASHOP=:1.7-7.2-apache


### Ejecutar

`docker-compose up -d`


### Accesible desde ...

`http://localhost:8079`


# Tags para Prestashop ...

[Docker Hub Prestashop](https://hub.docker.com/r/prestashop/prestashop/)

[Docker Hub MariaDB](https://hub.docker.com/_/mariadb)
