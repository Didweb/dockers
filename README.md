# Mis contenedores

Los contenedores que utilizo para el desarrollo de aplicaciones web.

---

## Symfony

Este genera 3 contenedores:

- Mariadb.
- PhpMyAdmin
- PHP 7.3 + apache

Genera 2 volúmenes:

- **data**: Para la base de datos.
- **my-app**: Para la aplicación Symfony

---

## Aplicación PHP + MariaDB + Apache

Genera 3 contenedores:

- Mariadb
- PhpMyAdmin
- PHP  7.2 + Apache

Genera 2 volúmenes  para datos y 1 para PhpMyAdmin:

- **data**: Para base de datos.
- **html**: Para los archivos PHP.
- **sessions**: Para PhpMyAdmin.

---

## Aplicación Prestashop

Genera 3 contenedores:

- Mariadb
- PhpMyAdmin
- Prestashop 1.7 + php 7.2 + Apache

Genera 2 volúmenes  para datos y 1 para PhpMyAdmin:

- **data**: Para base de datos.
- **html**: Para los archivos de Prestashop.
- **sessions**: Para PhpMyAdmin.

---

## Aplicación Wordpress

Consta de 3 contenedores:

- MariaDB
- PhpMyAdmin
- Wordpress last  + php last + Apache

Genera 2 volúmenes para datos y 1 para PhpMyAdmin:

- **data**: Para base de datos.
- **html**: Para los archivos de Wordpress.
- **sessions**: Para PhpMyAdmin.
