# Docker para Wordpress

Consta de 3 contenedores:

- MariaDB
- PhpMyAdmin
- Wordpress last  + php last + Apache

Genera 2 volúmenes para datos y 1 para phpmyadmin:

- **data**: Para base de datos.
- **html**: Para los archivos de Wordpress.
- **sessions**: Para PhpMyAdmin.


## Configuración

Existe el archivo `.env` donde podremos configurar los valores:

Cambia los valores, si crees necesario ...

- MYSQL_ROOT_PASSWORD=123456
- MYSQL_DATABASE=name_db
- MYSQL_USER=myuser
- MYSQL_PASSWORD=123456
- PORT_DB=3311
- PORT_PHPMYADMIN=8011
- PORT_WORDPRESS=8080
- TAG_WORDPRESS=:5.3-php7.1-apache

### Ejecutar

`docker-compose up -d`


### Accesible desde ...

`http://localhost:8010`


# Tags para Wordpress ...

[Docker Hub Wordpress](https://hub.docker.com/_/wordpress)

[Docker Hub MariaDB](https://hub.docker.com/_/mariadb)
