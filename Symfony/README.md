# Docker para Symfony

Este docker genera los siguientes contenedores:

- **db**: Mariadb
- **phpmyadminmy-app**: un PhpMyAdmin para consultar la base de datos.
- **phpmy-app**: PHP 7.3 + apache

### Cambiar la versión de PHP

Para cambiar la versión de PHP se ha de modificar el archivo `Docker/php/Dockerfile` es que se ejecuta al hacer `docker-compose build`.

hay que sustituir la línea ...

```
FROM php:7-apache
```

Por el tag deseado, pro ejemplo, para cargar el php 7.2...

```
FROM php:7.2-apache
```

## Construir

Por lo tanto el proceso la primera vez es...

- `docker-compose build`
- `docker-compose up -d`

Una vez tenemos la imagen de nuestro php ya solamente necesitaremos ...

- `docker-compose up -d`


### Accesible desde ...

`http://localhost:8081`


## Configuración de apache

En el directorio `apache` esta la configuración de apache.


## Archivos que necesitan Configuración

Algunos archivos necesitan ser configurados como los nombres de contenedores, puertos, etc...

- `docker-compose.yaml`
- `apache/000-default.php`

## Volúmenes

- **data**: Base de datos.
- **sessions**: Para phpmyadmin
- **my-app**: El proyecto Symfony




## Ejecutar comandos en el contenedor.

Para ejecutar comandos dentro del docker por ejemplo . `doctrine:fixtures:load` entre otras ...

```
docker exec ID_CONTAINER bin/console doctrine:fixtures:load

```

## Configurar Base de datos en Symfony

Configuración de la base de datos para Symfony en el archivo `.env` ...

```
DATABASE_URL=mysql://root:123456@db/my-app-database

```
## Configuración

Existe el archivo `.env` donde podremos configurar algunos valores:

Cambia los valores, si crees necesario ...

- MYSQL_ROOT_PASSWORD=123456
- MYSQL_DATABASE=name_db
- MYSQL_USER=myuser
- MYSQL_PASSWORD=123456
- PORT_DB=3311
- PORT_PHPMYADMIN=8011
- PORT_SYMFONY=8080


# Tags para PHP  ...

[Docker Hub PHP](https://hub.docker.com/_/php/)

[Docker Hub MariaDB](https://hub.docker.com/_/mariadb)
